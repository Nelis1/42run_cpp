/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   global_variables.hpp                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/30 14:51:47 by cnolte            #+#    #+#             */
/*   Updated: 2018/01/10 15:45:52 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GLOBAL_VARIABLES_HPP
# define GLOBAL_VARIABLES_HPP

extern int	game_menu;

extern GLuint	matricesUniBuffer;
extern GLuint	materialUniLoc;
extern GLuint	matricesUniLoc;
extern GLuint	program;

extern map<string, GLuint> textureIdMap;

extern vector <t_mesh_data>	mesh_world_1;
extern vector <t_mesh_data>	mesh_world_2;
extern vector <t_mesh_data>	mesh_imac[MAC_NS];
extern vector <t_mesh_data>	mesh_table_double[TBL_D_NS];
extern vector <t_mesh_data>	mesh_table_triple[TBL_T_NS];
extern vector <t_mesh_data>	mesh_chair[CHAIR_NS];
extern vector <t_mesh_data>	mesh_player_run[31];
extern vector <t_mesh_data>	mesh_player_jump[34];
extern vector <t_mesh_data>	mesh_player_fall[77];
extern vector <t_mesh_data>	mesh_wtc;
extern vector <t_mesh_data>	mesh_menu_died_quit_bg;
extern vector <t_mesh_data>	mesh_menu_died_retry_bg;
extern vector <t_mesh_data>	mesh_menu_quit_bg;
extern vector <t_mesh_data>	mesh_menu_start_bg;

extern int	game_menu;
extern int	rot;
extern int	rot_play;
extern int	move_east_west;
extern int	move_north_south;
extern int	h_neg;
extern int	v_neg;
extern int	jump;
extern int	next_turn;
extern int	dir;

extern const aiScene *world_1;
extern const aiScene *world_2;
extern const aiScene *imac[MAC_NS];
extern const aiScene *table_double[TBL_D_NS];
extern const aiScene *table_triple[TBL_T_NS];
extern const aiScene *chair[CHAIR_NS];
extern const aiScene *player_run[31];
extern const aiScene *player_jump[34];
extern const aiScene *player_fall[77];
extern const aiScene *wtc;
extern const aiScene *menu_died_quit_bg;
extern const aiScene *menu_died_retry_bg;
extern const aiScene *menu_quit_bg;
extern const aiScene *menu_start_bg;

extern GLFWwindow	*window;

extern int		score;
extern float	game_speed;
extern int		p_pos;
extern int		fall;
extern t_audio	audio;

extern GLuint	vertexLoc;
extern GLuint	normalLoc;
extern GLuint	texCoordLoc;

extern float	model_matrix[16];

#endif
