#ifndef FTRUN_HPP
# define FTRUN_HPP

using namespace std;

# include <IL/il.h>

# if (__APPLE__)
#  include <GL/glew.h>
#  include <openGL/gl.h>
#  include <GLFW/glfw3.h>
# endif

# include <SDL2/SDL.h>
# include <SDL2/SDL_mixer.h>
# include <assimp/scene.h>
# include <assimp/postprocess.h>
# include "assimp/Importer.hpp"

# include <iostream>
# include <fstream>
# include <map>
# include <vector>
# include <pthread.h>

# define AUDIO_RATE 44100
# define AUDIO_FORMAT AUDIO_S16SYS
# define AUDIO_CHANNELS 2
# define AUDIO_BUFFERS 1024

# define WIN_CAPTION "42run - cnolte"
# define SCR_W 1027
# define SCR_H 768

# define MULTITHREADING 1
# define DEBUG 0
# define DEBUG_DIE 1

# define ROT_SPEED 10

#define MAC_N 5
#define TBL_D_N 2
#define TBL_T_N 5
#define CHAIR_N 5

#define MAC_S 32
#define TBL_D_S 16
#define TBL_T_S 0
#define CHAIR_S 32

#define MAC_NS MAC_N + MAC_S
#define TBL_D_NS TBL_D_N + TBL_D_S
#define TBL_T_NS TBL_T_N + TBL_T_S
#define CHAIR_NS CHAIR_N + CHAIR_S

# include "structs.hpp"

# include "prototypes.hpp"

# include "global_variables.hpp"

#endif