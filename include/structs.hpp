#ifndef STRUCTS_HPP
# define STRUCTS_HPP

typedef struct	s_audio
{
	Mix_Chunk	*sound;
	Mix_Chunk	*sound1;
	Mix_Chunk	*sound2;
	Mix_Chunk	*sound3;
}				t_audio;

typedef struct	s_mesh_data
{
	GLuint	vao;
	GLuint	texIndex;
	GLuint	uniformBlockIndex;
	int		numFaces;
}				t_mesh_data;

typedef struct	s_mat_data{

	float	diffuse[4];
	float	ambient[4];
	float	specular[4];
	float	emissive[4];
	float	shininess;
	int		texCount;
}				t_mat_data;

typedef struct	s_hi_score
{
	string	name;
	int		score;
	string	meter;
}				t_hi_score;

#endif
