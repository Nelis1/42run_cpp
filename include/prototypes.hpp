#ifndef PROTOTYPES_HPP
# define PROTOTYPES_HPP

int		init_all(t_audio *audio);

void	renderScene(void);
void	create_projection_matrix(int w, int h);

int		load_set_opengl_data(void);
void	initial_move(void);
void	add_objects_to_gpu_memory(void);
void	load_textures(void);
GLuint	setup_shader(void);

void	renderScene(void);
void	set_model_matrix(void);
void	push_matrix();
void	pop_matrix();
void	mult_matrix(float *a, float *b);
void	set_camera(float posX, float posY, float posZ, float lookAtX, float lookAtY, float lookAtZ);
void	set_identity_matrix(float *mat, int size);
void	rotate(float angle, float x, float y, float z);

void	process_left_turn(int r);
void	process_right_turn(int r);
void	recursive_render (const aiScene *sc, const aiNode* nd, vector<t_mesh_data> meshes);
int		test_die_conditions(int j, int *f);

void	process_keys(GLFWwindow* window, int key, int scancode, int action, int mods);
void	process_up_key(void);
void	process_down_key(void);
void	process_left_key(void);
void	process_right_key(void);
void	process_return_key(void);

int		load_set_opengl_data(void);

void	forward_movement(void);
void	move(const aiScene **tmp, float x, float y, float z);
void	*moveThread(void *ii);
void	update_vao(const aiScene *sc, vector<t_mesh_data> *meshes);

void	move_north_func(void);
void	move_east_func(void);
void	move_south_func(void);
void	move_west_func(void);

void	exit_func(void);

#endif
