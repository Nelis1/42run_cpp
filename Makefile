NAME = 42run

CC = g++ -w -std=c++11

OS := $(shell uname)

CFLAGS = -lSDL2 -lfreetype -lpthread -lIL -lglfw -I ~/.brew/include/freetype2 -lassimp -lGLEW -o $(NAME) -I libft/ -I include/
CFLAGS += -I ~/.brew/include -L ~/.brew/lib -I ~/.brew/include -L ~/.brew/lib -framework OpenGL

CFILES = ~/.brew/lib/libSDL2_mixer.dylib source/main.cpp source/init_all.cpp source/movement/forward.cpp source/exit_func.cpp
CFILES += source/movement/move_north.cpp source/movement/move_east.cpp source/movement/move_south.cpp source/movement/move_west.cpp
CFILES += source/key_events/main_key_event.cpp source/key_events/process_down_key.cpp source/key_events/process_left_key.cpp
CFILES += source/key_events/process_return_key.cpp source/key_events/process_right_key.cpp source/key_events/process_up_key.cpp
CFILES += source/initial_move.cpp source/add_to_gpu.cpp source/load_textures.cpp source/load_props.cpp source/setup_shaders.cpp
CFILES += source/rendering.cpp source/matrices.cpp source/die_conditions.cpp source/movement/process_turn.cpp

$(NAME):
	$(CC) $(CFILES) $(CFLAGS)

all: $(NAME)

fclean:
	rm -rf $(NAME)

re: fclean all

co:
	rm -rf $(NAME)
	$(CC) $(CFILES) $(CFLAGS)

.PHONY: co re fclean all
