/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exit_func.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/29 00:38:13 by cnolte            #+#    #+#             */
/*   Updated: 2018/01/09 18:27:12 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ftrun.hpp"

static void	free_menu_items(void)
{
	for (unsigned int i = 0; i < mesh_menu_died_quit_bg.size(); ++i)
	{
		glDeleteVertexArrays(1,&(mesh_menu_died_quit_bg[i].vao));
		glDeleteTextures(1,&(mesh_menu_died_quit_bg[i].texIndex));
		glDeleteBuffers(1,&(mesh_menu_died_quit_bg[i].uniformBlockIndex));
	}
	for (unsigned int i = 0; i < mesh_menu_died_retry_bg.size(); ++i)
	{
		glDeleteVertexArrays(1,&(mesh_menu_died_retry_bg[i].vao));
		glDeleteTextures(1,&(mesh_menu_died_retry_bg[i].texIndex));
		glDeleteBuffers(1,&(mesh_menu_died_retry_bg[i].uniformBlockIndex));
	}
	for (unsigned int i = 0; i < mesh_menu_quit_bg.size(); ++i)
	{
		glDeleteVertexArrays(1,&(mesh_menu_quit_bg[i].vao));
		glDeleteTextures(1,&(mesh_menu_quit_bg[i].texIndex));
		glDeleteBuffers(1,&(mesh_menu_quit_bg[i].uniformBlockIndex));
	}
	for (unsigned int i = 0; i < mesh_menu_start_bg.size(); ++i)
	{
		glDeleteVertexArrays(1,&(mesh_menu_start_bg[i].vao));
		glDeleteTextures(1,&(mesh_menu_start_bg[i].texIndex));
		glDeleteBuffers(1,&(mesh_menu_start_bg[i].uniformBlockIndex));
	}
}

static void	free_animation(void)
{
	for (int j = 0; j < 31; j++)
	{
		for (unsigned int i = 0; i < mesh_player_run[j].size(); ++i)
		{
			glDeleteVertexArrays(1,&(mesh_player_run[j][i].vao));
			glDeleteTextures(1,&(mesh_player_run[j][i].texIndex));
			glDeleteBuffers(1,&(mesh_player_run[j][i].uniformBlockIndex));
		}
	}
	for (int j = 0; j < 34; j++)
	{
		for (unsigned int i = 0; i < mesh_player_jump[j].size(); ++i)
		{
			glDeleteVertexArrays(1,&(mesh_player_jump[j][i].vao));
			glDeleteTextures(1,&(mesh_player_jump[j][i].texIndex));
			glDeleteBuffers(1,&(mesh_player_jump[j][i].uniformBlockIndex));
		}
	}
	for (int j = 0; j < 77; j++)
	{
		for (unsigned int i = 0; i < mesh_player_fall[j].size(); ++i)
		{
			glDeleteVertexArrays(1,&(mesh_player_fall[j][i].vao));
			glDeleteTextures(1,&(mesh_player_fall[j][i].texIndex));
			glDeleteBuffers(1,&(mesh_player_fall[j][i].uniformBlockIndex));
		}
	}
}

static void	free_objects(void)
{
	for (int j = 0; j < MAC_NS; j++)
	{
		for (unsigned int i = 0; i < mesh_imac[j].size(); ++i)
		{
			glDeleteVertexArrays(1,&(mesh_imac[j][i].vao));
			glDeleteTextures(1,&(mesh_imac[j][i].texIndex));
			glDeleteBuffers(1,&(mesh_imac[j][i].uniformBlockIndex));
		}
	}
	for (int j = 0; j < TBL_D_NS; j++)
	{
		for (unsigned int i = 0; i < mesh_table_double[j].size(); ++i)
		{
			glDeleteVertexArrays(1,&(mesh_table_double[j][i].vao));
			glDeleteTextures(1,&(mesh_table_double[j][i].texIndex));
			glDeleteBuffers(1,&(mesh_table_double[j][i].uniformBlockIndex));
		}
	}
	for (int j = 0; j < TBL_T_NS; j++)
	{
		for (unsigned int i = 0; i < mesh_table_triple[j].size(); ++i)
		{
			glDeleteVertexArrays(1,&(mesh_table_triple[j][i].vao));
			glDeleteTextures(1,&(mesh_table_triple[j][i].texIndex));
			glDeleteBuffers(1,&(mesh_table_triple[j][i].uniformBlockIndex));
		}
	}
	for (int j = 0; j < CHAIR_NS; j++)
	{
		for (unsigned int i = 0; i < mesh_chair[j].size(); ++i)
		{
			glDeleteVertexArrays(1,&(mesh_chair[j][i].vao));
			glDeleteTextures(1,&(mesh_chair[j][i].texIndex));
			glDeleteBuffers(1,&(mesh_chair[j][i].uniformBlockIndex));
		}
	}
}

static void	free_worlds()
{
	for (unsigned int i = 0; i < mesh_world_1.size(); ++i)
	{
		glDeleteVertexArrays(1,&(mesh_world_1[i].vao));
		glDeleteTextures(1,&(mesh_world_1[i].texIndex));
		glDeleteBuffers(1,&(mesh_world_1[i].uniformBlockIndex));
	}
	for (unsigned int i = 0; i < mesh_world_2.size(); ++i)
	{
		glDeleteVertexArrays(1,&(mesh_world_2[i].vao));
		glDeleteTextures(1,&(mesh_world_2[i].texIndex));
		glDeleteBuffers(1,&(mesh_world_2[i].uniformBlockIndex));
	}
}

void	exit_func(void)
{
	textureIdMap.clear();
	free_worlds();
	free_objects();
	free_animation();
	for (unsigned int i = 0; i < mesh_wtc.size(); ++i)
	{
		glDeleteVertexArrays(1,&(mesh_wtc[i].vao));
		glDeleteTextures(1,&(mesh_wtc[i].texIndex));
		glDeleteBuffers(1,&(mesh_wtc[i].uniformBlockIndex));
	}
	free_menu_items();
	glDeleteBuffers(1,&matricesUniBuffer);
}
