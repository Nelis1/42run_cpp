/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/29 00:38:50 by cnolte            #+#    #+#             */
/*   Updated: 2018/01/10 16:22:13 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ftrun.hpp"

t_audio	audio;

int	game_menu = 1;
int	rot = 0;
int	rot_play = 0;
int	move_east_west = 0;
int	move_north_south = 9;
int	h_neg = 0;
int	v_neg = 0;
int	jump = 0;
int	fall = 0;
int p_pos = 0;
int	next_turn = -1;
int dir = 0;
int	score = 0;
float	game_speed = 0.05;
GLuint	program;

vector <t_mesh_data>	mesh_world_1;
vector <t_mesh_data>	mesh_world_2;
vector <t_mesh_data>	mesh_imac[MAC_NS];
vector <t_mesh_data>	mesh_table_double[TBL_D_NS];
vector <t_mesh_data>	mesh_table_triple[TBL_T_NS];
vector <t_mesh_data>	mesh_chair[CHAIR_NS];
vector <t_mesh_data>	mesh_player_run[31];
vector <t_mesh_data>	mesh_player_jump[34];
vector <t_mesh_data>	mesh_player_fall[77];
vector <t_mesh_data>	mesh_wtc;
vector <t_mesh_data>	mesh_menu_died_quit_bg;
vector <t_mesh_data>	mesh_menu_died_retry_bg;
vector <t_mesh_data>	mesh_menu_quit_bg;
vector <t_mesh_data>	mesh_menu_start_bg;
vector <t_hi_score>		hi_scores;

const aiScene *world_1 = NULL;
const aiScene *world_2 = NULL;
const aiScene *imac[MAC_NS];
const aiScene *table_double[TBL_D_NS];
const aiScene *table_triple[TBL_T_NS];
const aiScene *chair[CHAIR_NS];
const aiScene *player_run[31];
const aiScene *player_jump[34];
const aiScene *player_fall[77];
const aiScene *wtc = NULL;
const aiScene *menu_died_quit_bg = NULL;
const aiScene *menu_died_retry_bg = NULL;
const aiScene *menu_quit_bg = NULL;
const aiScene *menu_start_bg = NULL;
GLFWwindow	*window;

// Vertex Attribute Locations
GLuint	vertexLoc = 0;
GLuint	normalLoc = 1;
GLuint	texCoordLoc = 2;
GLuint	materialUniLoc = 2;
GLuint	matricesUniLoc = 1;
// images / texture
// map image filenames to textureIds
// pointer to texture Array
map<string, GLuint> textureIdMap;

GLuint*	textureIds = NULL;// pointer to texture Array

static void	process_highscore(char **argv)
{
	vector <t_hi_score>::iterator	tmp_vector;
	t_hi_score						tmp_score;
	
	ifstream infile;
	infile.open("highscores.txt");
	t_hi_score tmp;
	while (infile >> tmp.name >> tmp.score >> tmp.meter)
		hi_scores.push_back(tmp);
	infile.close();
	ofstream outputFile;
	outputFile.open("highscores.txt");
	int printed = 0;
	for (int k = 0; k < hi_scores.size(); k++)
	{
		if (score > hi_scores[k].score && printed == 0)
		{
			cout << argv[1] << " " << to_string(score) << " Meters" << endl;
			outputFile << argv[1] << " " << to_string(score) << " Meters" << endl;
			printed = 1;
		}
		else
		{
			cout << hi_scores[k].name << " " << to_string(hi_scores[k].score) << " Meters" << endl;
			outputFile << hi_scores[k].name << " " << to_string(hi_scores[k].score) << " Meters" << endl;
		}
	}
	if (printed == 0)
		outputFile << argv[1] << " " << to_string(score) << " Meters" << endl;
	outputFile.close();
}

int main(int argc, char **argv)
{
	float	game_frame_limiter;
	
	game_frame_limiter = 0;
	if (argc == 2)
	{
		if (init_all(&audio))
			return (-1);
		if (load_set_opengl_data())
		{
			printf("Could not Load the Model\n");
			return (-1);
		}
		create_projection_matrix(SCR_W, SCR_H);
		glfwSetKeyCallback(window, process_keys);
		while (!glfwWindowShouldClose(window))
		{
			game_frame_limiter += game_speed;
			if (game_frame_limiter > 100)
			{
				renderScene();
				game_frame_limiter = 0;
			}
			glfwPollEvents();
		}
		glfwDestroyWindow(window);
		glfwTerminate();
		exit_func();
		process_highscore(argv);
	}
	else if (argc > 2)
		cout << "please enter only one name as argument." << endl;
	else
		cout << "Please enter a name as argument." << endl;
	return(0);
}
