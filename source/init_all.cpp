/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_all.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/28 23:50:15 by cnolte            #+#    #+#             */
/*   Updated: 2018/01/10 14:47:04 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ftrun.hpp"

static void	init_importers(void)
{
	for (int i = 0; i <= 30; i++)
		player_run[i] = NULL;
	for (int i = 0; i <= 33; i++)
		player_jump[i] = NULL;
	for (int i = 0; i <= 76; i++)
		player_fall[i] = NULL;
	for (int i = 0; i < TBL_D_NS; i++)
		table_double[i] = NULL;
	for (int i = 0; i < TBL_T_NS; i++)
		table_triple[i] = NULL;
	for (int i = 0; i < CHAIR_NS; i++)
		chair[i] = NULL;
	for (int i = 0; i < MAC_NS; i++)
		imac[i] = NULL;
}

static int	init_glfw(void)
{
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
	window = glfwCreateWindow(SCR_W, SCR_H, WIN_CAPTION, nullptr, nullptr);
	int screenWidth, screenHeight;
	glfwGetFramebufferSize(window, &screenWidth, &screenHeight);
	if (window == nullptr)
	{
		cout << "Failed to create GLFW window" << endl;
		glfwTerminate();
		return (-1);
	}
	glfwMakeContextCurrent(window);
	glfwSwapInterval(1);
	return (0);
}

static int	init_audio(t_audio *audio)
{
	if (Mix_OpenAudio(AUDIO_RATE, AUDIO_FORMAT, AUDIO_CHANNELS,
			AUDIO_BUFFERS) != 0)
		return (-1);
	audio->sound = NULL;
	audio->sound = Mix_LoadWAV("resources/sounds/here_we_go.wav");
	if (audio->sound == NULL)
		return (-1);
	audio->sound1 = NULL;
	audio->sound1 = Mix_LoadWAV("resources/sounds/bg_music.wav");
	if (audio->sound1 == NULL)
		return (-1);
	audio->sound2 = NULL;
	audio->sound2 = Mix_LoadWAV("resources/sounds/go.wav");
	if (audio->sound2 == NULL)
		return (-1);
	audio->sound3 = NULL;
	audio->sound3 = Mix_LoadWAV("resources/sounds/countdown.wav");
	if (audio->sound3 == NULL)
		return (-1);
	SDL_ClearError();
	return (0);
}

int	init_all(t_audio *audio)
{
	SDL_Init(SDL_INIT_AUDIO);
	if (init_audio(audio))
		return (-1);
	if (init_glfw())
		return (-1);
	if (glewInit() != GLEW_OK)
	{
		cout << "Failed to initialize GLEW" << endl;
		return (-1);
	}
	if (glewIsSupported("GL_VERSION_4_1"))
		printf("Ready for OpenGL 4.1\n");
	else
	{
		printf("OpenGL 4.1 not supported\n");
		return(-1);
	}
	init_importers();
	return (0);
}
