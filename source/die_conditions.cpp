/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   die_conditions.cpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/10 15:58:49 by cnolte            #+#    #+#             */
/*   Updated: 2018/01/10 16:04:38 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ftrun.hpp"

int	test_die_conditions(int j, int *f)
{
	if (move_north_south == 15 && (p_pos == 1 || (p_pos == 0 && j < 10)) && fall == 0)
	{
		if (DEBUG_DIE == 1)
		{
			fall = 1;
			*f = 0;
		}
	}
	else if (move_north_south == 25 && p_pos == -1 && fall == 0)
	{
		if (DEBUG_DIE == 1)
		{
			fall = 1;
			*f = 0;
		}
	}
	else if (move_north_south == 35 && (p_pos == -1 || p_pos == 0) && fall == 0)
	{
		if (DEBUG_DIE == 1)
		{
			fall = 1;
			*f = 0;
		}
	}
	else if (move_north_south == 45 && (p_pos == 0 || p_pos == 1) && fall == 0)
	{
		if (DEBUG_DIE == 1)
		{
			fall = 1;
			*f = 0;
		}
	}
	else if (move_north_south == 55 && (p_pos == 0 || p_pos == 1) && fall == 0)
	{
		if (DEBUG_DIE == 1)
		{
			fall = 1;
			*f = 0;
		}
	}
	else if (move_north_south == 65 && (p_pos == -1 || p_pos == 0 || p_pos == 1) && fall == 0 && j < 10)
	{
		if (DEBUG_DIE == 1)
		{
			fall = 1;
			*f = 0;
		}
	}
}