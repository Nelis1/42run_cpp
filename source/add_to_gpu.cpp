/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   add_to_gpu.cpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/10 13:32:33 by cnolte            #+#    #+#             */
/*   Updated: 2018/01/10 14:45:23 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ftrun.hpp"

static void	set_float4(float f[4], float a, float b, float c, float d)
{
	f[0] = a;
	f[1] = b;
	f[2] = c;
	f[3] = d;
}

static void	color4_to_float4(const aiColor4D *c, float f[4])
{
	f[0] = c->r;
	f[1] = c->g;
	f[2] = c->b;
	f[3] = c->a;
}

static void	gen_vao_and_uniformbuf(const aiScene *sc, vector<t_mesh_data> *meshes)
{
	t_mesh_data	tmp_mesh;
	t_mat_data	tmp_mat;
	GLuint		buffer;
	
	for (unsigned int n = 0; n < sc->mNumMeshes; ++n)
	{
		const aiMesh* mesh = sc->mMeshes[n];
		unsigned int *faceArray;
		faceArray = (unsigned int *)malloc(sizeof(unsigned int) * mesh->mNumFaces * 3);
		unsigned int faceIndex = 0;
		for (unsigned int t = 0; t < mesh->mNumFaces; ++t) {
			const aiFace* face = &mesh->mFaces[t];

			memcpy(&faceArray[faceIndex], face->mIndices,3 * sizeof(unsigned int));
			faceIndex += 3;
		}
		tmp_mesh.numFaces = sc->mMeshes[n]->mNumFaces;
		glGenVertexArrays(1,&(tmp_mesh.vao));
		glBindVertexArray(tmp_mesh.vao);
		glGenBuffers(1, &buffer);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * mesh->mNumFaces * 3, faceArray, GL_STATIC_DRAW);
		if (mesh->HasPositions())
		{
			glGenBuffers(1, &buffer);
			glBindBuffer(GL_ARRAY_BUFFER, buffer);
			glBufferData(GL_ARRAY_BUFFER, sizeof(float)*3*mesh->mNumVertices, mesh->mVertices, GL_STATIC_DRAW);
			glEnableVertexAttribArray(vertexLoc);
			glVertexAttribPointer(vertexLoc, 3, GL_FLOAT, 0, 0, 0);
		}
		if (mesh->HasNormals()) {
			glGenBuffers(1, &buffer);
			glBindBuffer(GL_ARRAY_BUFFER, buffer);
			glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 3 * mesh->mNumVertices, mesh->mNormals, GL_STATIC_DRAW);
			glEnableVertexAttribArray(normalLoc);
			glVertexAttribPointer(normalLoc, 3, GL_FLOAT, 0, 0, 0);
		}
		if (mesh->HasTextureCoords(0)) {
			float *texCoords = (float *)malloc(sizeof(float)*2*mesh->mNumVertices);
			for (unsigned int k = 0; k < mesh->mNumVertices; ++k)
			{
				texCoords[k*2]   = mesh->mTextureCoords[0][k].x;
				texCoords[k*2+1] = mesh->mTextureCoords[0][k].y; 
				
			}
			glGenBuffers(1, &buffer);
			glBindBuffer(GL_ARRAY_BUFFER, buffer);
			glBufferData(GL_ARRAY_BUFFER, sizeof(float)*2*mesh->mNumVertices, texCoords, GL_STATIC_DRAW);
			glEnableVertexAttribArray(texCoordLoc);
			glVertexAttribPointer(texCoordLoc, 2, GL_FLOAT, 0, 0, 0);
		}
		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER,0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0);
		aiMaterial *mtl = sc->mMaterials[mesh->mMaterialIndex];
		aiString texPath;
		if(AI_SUCCESS == mtl->GetTexture(aiTextureType_DIFFUSE, 0, &texPath))
		{
			unsigned int texId = textureIdMap[texPath.data];
			tmp_mesh.texIndex = texId;
			tmp_mat.texCount = 1;
		}
		else
			tmp_mat.texCount = 0;
		float c[4];
		set_float4(c, 0.8f, 0.8f, 0.8f, 1.0f);
		aiColor4D diffuse;
		if(AI_SUCCESS == aiGetMaterialColor(mtl, AI_MATKEY_COLOR_DIFFUSE, &diffuse))
			color4_to_float4(&diffuse, c);
		memcpy(tmp_mat.diffuse, c, sizeof(c));
		set_float4(c, 0.2f, 0.2f, 0.2f, 1.0f);
		aiColor4D ambient;
		if(AI_SUCCESS == aiGetMaterialColor(mtl, AI_MATKEY_COLOR_AMBIENT, &ambient))
			color4_to_float4(&ambient, c);
		memcpy(tmp_mat.ambient, c, sizeof(c));
		set_float4(c, 0.0f, 0.0f, 0.0f, 1.0f);
		aiColor4D specular;
		if(AI_SUCCESS == aiGetMaterialColor(mtl, AI_MATKEY_COLOR_SPECULAR, &specular))
			color4_to_float4(&specular, c);
		memcpy(tmp_mat.specular, c, sizeof(c));
		set_float4(c, 0.0f, 0.0f, 0.0f, 1.0f);
		aiColor4D emission;
		if(AI_SUCCESS == aiGetMaterialColor(mtl, AI_MATKEY_COLOR_EMISSIVE, &emission))
			color4_to_float4(&emission, c);
		memcpy(tmp_mat.emissive, c, sizeof(c));
		float shininess = 0.0;
		unsigned int max;
		aiGetMaterialFloatArray(mtl, AI_MATKEY_SHININESS, &shininess, &max);
		tmp_mat.shininess = shininess;
		glGenBuffers(1,&(tmp_mesh.uniformBlockIndex));
		glBindBuffer(GL_UNIFORM_BUFFER,tmp_mesh.uniformBlockIndex);
		glBufferData(GL_UNIFORM_BUFFER, sizeof(tmp_mat), (void *)(&tmp_mat), GL_STATIC_DRAW);
		meshes->push_back(tmp_mesh);
	}
}

void	add_objects_to_gpu_memory(void)
{
	gen_vao_and_uniformbuf(world_1, &mesh_world_1);
	gen_vao_and_uniformbuf(world_2, &mesh_world_2);
	gen_vao_and_uniformbuf(wtc, &mesh_wtc);
	gen_vao_and_uniformbuf(menu_died_quit_bg, &mesh_menu_died_quit_bg);
	gen_vao_and_uniformbuf(menu_died_retry_bg, &mesh_menu_died_retry_bg);
	gen_vao_and_uniformbuf(menu_quit_bg, &mesh_menu_quit_bg);
	gen_vao_and_uniformbuf(menu_start_bg, &mesh_menu_start_bg);
	for (int m = 0; m < MAC_NS; m++)
		gen_vao_and_uniformbuf(imac[m], &mesh_imac[m]);
	for (int m = 0; m < TBL_D_NS; m++)
		gen_vao_and_uniformbuf(table_double[m], &mesh_table_double[m]);
	for (int m = 0; m < TBL_T_NS; m++)
		gen_vao_and_uniformbuf(table_triple[m], &mesh_table_triple[m]);
	for (int m = 0; m < CHAIR_NS; m++)
		gen_vao_and_uniformbuf(chair[m], &mesh_chair[m]);
	for (int m = 0; m <= 30; m++)
		gen_vao_and_uniformbuf(player_run[m], &mesh_player_run[m]);
	for (int m = 0; m <= 33; m++)
		gen_vao_and_uniformbuf(player_jump[m], &mesh_player_jump[m]);
	for (int m = 0; m <= 77; m++)
		gen_vao_and_uniformbuf(player_fall[m], &mesh_player_fall[m]);
}