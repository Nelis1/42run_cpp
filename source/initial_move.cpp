/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   initial_move.cpp                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/10 13:17:54 by cnolte            #+#    #+#             */
/*   Updated: 2018/02/13 18:19:13 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ftrun.hpp"

static void	move_south_props_left(void)
{
	move(&imac[5], -70, -8, -681.2);
	move(&imac[6], -120, -8, -681.2);
	move(&table_double[2], -100, -50, -681.2);
	move(&chair[5], -70, -60, -650.0);
	move(&chair[6], -130, -60, -650.0);

	move(&imac[9], -70, -8, -1313.2);
	move(&imac[10], -120, -8, -1313.2);
	move(&table_double[4], -100, -50, -1313.2);
	move(&chair[9], -70, -60, -1282.0);
	move(&chair[10], -130, -60, -1282.0);

	move(&imac[13], -70, -8, -1945.2);
	move(&imac[14], -120, -8, -1945.2);
	move(&table_double[6], -100, -50, -1945.2);
	move(&chair[13], -70, -60, -1914);
	move(&chair[14], -130, -60, -1914.0);

	move(&imac[17], -70, -8, -2577.2);
	move(&imac[18], -120, -8, -2577.2);
	move(&table_double[8], -100, -50, -2577.2);
	move(&chair[17], -70, -60, -2546.0);
	move(&chair[18], -130, -60, -2546.0);
	
	move(&imac[21], -70, -8, -3209.2);
	move(&imac[22], -120, -8, -3209.2);
	move(&table_double[10], -100, -50, -3209.2);
	move(&chair[21], -70, -60, -3178.0);
	move(&chair[22], -130, -60, -3178.0);

	move(&imac[25], -70, -8, -3841.2);
	move(&imac[26], -120, -8, -3841.2);
	move(&table_double[12], -100, -50, -3841.2);
	move(&chair[25], -70, -60, -3810.0);
	move(&chair[26], -130, -60, -3810.0);

	move(&imac[29], -70, -8, -4473.2);
	move(&imac[30], -120, -8, -4473.2);
	move(&table_double[14], -100, -50, -4473.2);
	move(&chair[29], -70, -60, -4442);
	move(&chair[30], -130, -60, -4442.0);

	move(&imac[33], -70, -8, -5105.2);
	move(&imac[34], -120, -8, -5105.2);
	move(&table_double[16], -100, -50, -5105.2);
	move(&chair[33], -70, -60, -5074.0);
	move(&chair[34], -130, -60, -5074.0);
}

static void	move_south_props_right(void)
{
	move(&imac[7], 70, -8, -681.2);
	move(&imac[8], 120, -8, -681.2);
	move(&table_double[3], 100, -50, -681.2);
	move(&chair[7], 70, -60, -650.0);
	move(&chair[8], 130, -60, -650.0);

	move(&imac[11], 70, -8, -1313.2);
	move(&imac[12], 120, -8, -1313.2);
	move(&table_double[5], 100, -50, -1313.2);
	move(&chair[11], 70, -60, -1282.0);
	move(&chair[12], 130, -60, -1282.0);

	move(&imac[15], 70, -8, -1945.2);
	move(&imac[16], 120, -8, -1945.2);
	move(&table_double[7], 100, -50, -1945.2);
	move(&chair[15], 70, -60, -1914);
	move(&chair[16], 130, -60, -1914.0);

	move(&imac[19], 70, -8, -2577.2);
	move(&imac[20], 120, -8, -2577.2);
	move(&table_double[9], 100, -50, -2577.2);
	move(&chair[19], 70, -60, -2546.0);
	move(&chair[20], 130, -60, -2546.0);

	move(&imac[23], 70, -8, -3209.2);
	move(&imac[24], 120, -8, -3209.2);
	move(&table_double[11], 100, -50, -3209.2);
	move(&chair[23], 70, -60, -3178.0);
	move(&chair[24], 130, -60, -3178.0);

	move(&imac[27], 70, -8, -3841.2);
	move(&imac[28], 120, -8, -3841.2);
	move(&table_double[13], 100, -50, -3841.2);
	move(&chair[27], 70, -60, -3810.0);
	move(&chair[28], 130, -60, -3810.0);

	move(&imac[31], 70, -8, -4473.2);
	move(&imac[32], 120, -8, -4473.2);
	move(&table_double[15], 100, -50, -4473.2);
	move(&chair[31], 70, -60, -4442.0);
	move(&chair[32], 130, -60, -4442.0);

	move(&imac[35], 70, -8, -5105.2);
	move(&imac[36], 120, -8, -5105.2);
	move(&table_double[17], 100, -50, -5105.2);
	move(&chair[35], 70, -60, -5074.0);
	move(&chair[36], 130, -60, -5074.0);
}

static void move_south_props(void)
{
	move_south_props_left();
	move_south_props_right();
}

static void	move_north_props(void)
{
	move(&imac[0], 70, -8, -681.2);
	move(&table_triple[0], 70, -50, -681.2);
	move(&chair[0], 80, -60, -650.0);

	move(&imac[1], -70, -8, -1313.2);
	move(&table_double[0], -100, -50, -1313.2);
	move(&chair[1], -80, -60, -1282.0);

	move(&imac[2], -70, -8, -1945.2);
	move(&table_triple[1], -70, -50, -1945.2);
	move(&chair[2], -80, -60, -1914.0);

	move(&imac[3], 70, -8, -2577.2);
	move(&table_triple[2], 70, -50, -2577.2);
	move(&chair[3], 80, -60, -2546.0);

	move(&imac[4], 70, -8, -3209.2);
	move(&table_triple[3], 70, -50, -3209.2);
	move(&chair[4], 80, -60, -3178.0);

	move(&table_double[1], -100, -50, -3841.2);
	move(&table_triple[4], 70, -50, -3841.2);
}

void	initial_move(void)
{
	move(&world_1, 0, 0, -5263.2);
	move(&world_2, 0, 0, -5885);
	move(&wtc, 99, 83, -160);
	move(&menu_died_quit_bg, 0, 40, -900);
	move(&menu_died_retry_bg, 0, 40, -900);
	move(&menu_quit_bg, 0, 40, -900);
	move(&menu_start_bg, 0, 40, -900);
	move_north_props();
	move_south_props();
}