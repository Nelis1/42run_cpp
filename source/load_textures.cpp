/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   load_textures.cpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/10 13:49:38 by cnolte            #+#    #+#             */
/*   Updated: 2018/01/10 13:56:11 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ftrun.hpp"

static int assign_texture(const aiScene *scene)
{
	ILboolean	success;

	ilInit();
	for (unsigned int m=0; m<scene->mNumMaterials; ++m)
	{
		int texIndex = 0;
		aiString	path;

		aiReturn texFound = scene->mMaterials[m]->GetTexture(aiTextureType_DIFFUSE, texIndex, &path);
		while (texFound == AI_SUCCESS)
		{
			textureIdMap[path.data] = 0;
			texIndex++;
			texFound = scene->mMaterials[m]->GetTexture(aiTextureType_DIFFUSE, texIndex, &path);
		}
	}
	int numTextures = textureIdMap.size();
	ILuint* imageIds = new ILuint[numTextures];
	ilGenImages(numTextures, imageIds); 
	GLuint* textureIds = new GLuint[numTextures];
	glGenTextures(numTextures, textureIds);
	map<string, GLuint>::iterator itr = textureIdMap.begin();
	int i = 0;
	for (; itr != textureIdMap.end(); ++i, ++itr)
	{
		string filename = (*itr).first;
		(*itr).second = textureIds[i];
		ilBindImage(imageIds[i]);
		ilEnable(IL_ORIGIN_SET);
		ilOriginFunc(IL_ORIGIN_LOWER_LEFT);
		success = ilLoadImage((ILstring)filename.c_str());
		if (success) {
			ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE);
			glBindTexture(GL_TEXTURE_2D, textureIds[i]);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, ilGetInteger(IL_IMAGE_WIDTH),
				ilGetInteger(IL_IMAGE_HEIGHT), 0, GL_RGBA, GL_UNSIGNED_BYTE,
				ilGetData());
		}
		else
			printf("Couldn't load Image: %s\n", filename.c_str());
	}
	ilDeleteImages(numTextures, imageIds); 
	delete [] imageIds;
	delete [] textureIds;
	return true;
}

void load_textures(void)
{
	assign_texture(world_1);
	assign_texture(world_2);
	for (int k = 0; k < MAC_NS; k++)
		assign_texture(imac[k]);
	for (int k = 0; k < TBL_D_NS; k++)
		assign_texture(table_double[k]);
	for (int k = 0; k < TBL_T_NS; k++)
		assign_texture(table_triple[k]);
	assign_texture(wtc);
	assign_texture(menu_died_quit_bg);
	assign_texture(menu_died_retry_bg);
	assign_texture(menu_quit_bg);
	assign_texture(menu_start_bg);
}
