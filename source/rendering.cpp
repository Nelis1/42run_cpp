/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rendering.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/10 15:03:31 by cnolte            #+#    #+#             */
/*   Updated: 2018/01/10 16:45:02 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ftrun.hpp"

float	model_matrix[16];
float	mytime = 0;
float	frame = 0;
float	old_time = 0;
float	cam_x = 0;
float	cam_y = 0;
float	cam_z = 0.0001;

void	move(const aiScene **tmp, float x, float y, float z)
{
	int	i;
	int	j;

	i = 0;
	while (i < (*tmp)->mNumMeshes)
	{
		j = 0;
		while (j < (*tmp)->mMeshes[i]->mNumVertices)
		{
			(*tmp)->mMeshes[i]->mVertices[j].x += x;
			(*tmp)->mMeshes[i]->mVertices[j].y += y;
			(*tmp)->mMeshes[i]->mVertices[j].z += z;
			j++;
		}
		i++;
	}
}

void *moveThread(void *ii)
{
	long i = (long)ii;
	if (i == 0)
		move(&world_1, 0, 0, 63.2);
	else if (i == 1)
		for (int i = 0; i < TBL_D_N; i++)
			move(&table_double[i], 0, 0, 63.2);
	else if (i == 2)
		for (int i = 0; i < TBL_T_N; i++)
			move(&table_triple[i], 0, 0, 63.2);
	else if (i == 3)
		for (int i = 0; i < MAC_N; i++)
			move(&imac[i], 0, 0, 63.2);
	else if (i == 4)
		for (int i = 0; i < CHAIR_N; i++)
			move(&chair[i], 0, 0, 63.2);
	pthread_exit(NULL);
}

void update_vao(const aiScene *sc, vector<t_mesh_data> *meshes)
{
	GLint			nAttr = 0;
	t_mesh_data		aMesh;

	for (unsigned int n = 0; n < sc->mNumMeshes; ++n)
	{
		const aiMesh* mesh = sc->mMeshes[n];
		glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &nAttr);
		glBindVertexArray((*meshes)[n].vao);
		for (int iAttr = 0; iAttr < nAttr; ++iAttr)
		{
			GLint	vboId = 0;
			glGetVertexAttribiv(vertexLoc, GL_VERTEX_ATTRIB_ARRAY_BUFFER_BINDING, &vboId);
			if (vboId > 0)
			{
				glBindBuffer(GL_ARRAY_BUFFER, vboId);
				glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(float)*3*mesh->mNumVertices, mesh->mVertices);
			}
		}
	}
}

void	recursive_render (const aiScene *sc, const aiNode* nd, vector<t_mesh_data> meshes)
{
	float	aux[16];
	// Get node transformation matrix
	aiMatrix4x4 m = nd->mTransformation;
	// OpenGL matrices are column major
	m.Transpose();
	// save model matrix and apply node transformation
	push_matrix();
	memcpy(aux,&m,sizeof(float) * 16);
	mult_matrix(model_matrix, aux);
	set_model_matrix();
	// draw all meshes assigned to this node
	for (unsigned int n=0; n < nd->mNumMeshes; ++n){
		// bind material uniform
		glBindBufferRange(GL_UNIFORM_BUFFER, materialUniLoc, meshes[nd->mMeshes[n]].uniformBlockIndex, 0, sizeof(t_mat_data));
		// bind texture
		glBindTexture(GL_TEXTURE_2D, meshes[nd->mMeshes[n]].texIndex);
		// bind VAO
		// meshes[nd->mMeshes[n]].vao.mMeshes.mVertices.x += 0.5;
		glBindVertexArray(meshes[nd->mMeshes[n]].vao);
		// draw
		glDrawElements(GL_TRIANGLES,meshes[nd->mMeshes[n]].numFaces*3,GL_UNSIGNED_INT,0);
	}
	// draw all children
	for (unsigned int n=0; n < nd->mNumChildren; ++n)
		recursive_render(sc, nd->mChildren[n], meshes);
	pop_matrix();
}

static void	render_south_props(void)
{
	for (int i = MAC_N; i < MAC_NS; i++)
		recursive_render(imac[i], imac[i]->mRootNode, mesh_imac[i]);
	for (int i = TBL_D_N; i < TBL_D_NS; i++)
		recursive_render(table_double[i], table_double[i]->mRootNode, mesh_table_double[i]);
	for (int i = TBL_T_N; i < TBL_T_NS; i++)
		recursive_render(table_triple[i], table_triple[i]->mRootNode, mesh_table_triple[i]);
	for (int i = CHAIR_N; i < CHAIR_NS; i++)
		recursive_render(chair[i], chair[i]->mRootNode, mesh_chair[i]);
}

static void	render_north_props(void)
{
	for (int i = 0; i < MAC_N; i++)
		recursive_render(imac[i], imac[i]->mRootNode, mesh_imac[i]);
	for (int i = 0; i < TBL_D_N; i++)
		recursive_render(table_double[i], table_double[i]->mRootNode, mesh_table_double[i]);
	for (int i = 0; i < TBL_T_N; i++)
		recursive_render(table_triple[i], table_triple[i]->mRootNode, mesh_table_triple[i]);
	for (int i = 0; i < CHAIR_N; i++)
		recursive_render(chair[i], chair[i]->mRootNode, mesh_chair[i]);
}

void	renderScene(void)
{
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f );
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	set_camera(cam_x, cam_y, cam_z, 0, 0, 0);
	// set the model matrix to the identity Matrix
	set_identity_matrix(model_matrix, 4);
	glUseProgram(program);
	if (game_menu == 0)
	{
		static int		run;
		static int		i;
		static int		r;
		static int		j;
		static int		f;
		static float	forward;

		if (rot == 3)
			process_right_turn(r);
		else if (rot == 2)
			process_left_turn(r);
		else if (rot == 0)
		{
			test_die_conditions(j, &f);
			rotate((float)dir, 0.0, 1.0, 0.0);
			recursive_render(world_1, world_1->mRootNode, mesh_world_1);
			recursive_render(world_2, world_2->mRootNode, mesh_world_2);
			rotate(-(float)dir, 0.0, 1.0, 0.0);
			if (rot_play == 0)
				render_north_props();
			else if (rot_play == 180 || rot_play == -180)
				render_south_props();
			recursive_render(wtc, wtc->mRootNode, mesh_wtc);
			if (fall == 0)
			{
				if (jump == 0)
				{
					if (r < 30)
					{
						r++;
						recursive_render(player_run[r], player_run[r]->mRootNode, mesh_player_run[r]);
					}
					else
					{
						recursive_render(player_run[0], player_run[0]->mRootNode, mesh_player_run[0]);
						r = 1;
					}
				}
				else if (jump == 1)
				{
					if (j <= 33)
					{
						recursive_render(player_jump[j], player_jump[j]->mRootNode, mesh_player_jump[j]);
						j++;
						if (j > 33)
						{
							j = 0;
							r = 0;
							jump = 0;
						}
					}
				}
			}
			else if (fall == 1)
			{
				if (f < 76)
					recursive_render(player_fall[f], player_fall[f]->mRootNode, mesh_player_fall[f]);
				else
					game_menu = 3;
				f++;
			}
		}
		if (fall == 0)
		{
			i += 5.0;
			if (i >= 10.0)
			{
				if (DEBUG == 0)
					forward_movement();
				i = 0.0;
			}
		}
		frame++;
		mytime = glfwGetTime();
		if (mytime - old_time >= 1)
		{
			printf("fps:%.2f\n", frame);
			old_time = mytime;
			frame = 0;
		}
	}
	else
	{
		if (game_menu == 1)
			recursive_render(menu_start_bg, menu_start_bg->mRootNode, mesh_menu_start_bg);
		else if (game_menu == 2)
			recursive_render(menu_quit_bg, menu_quit_bg->mRootNode, mesh_menu_quit_bg);
		else if (game_menu == 3)
			recursive_render(menu_died_retry_bg, menu_died_retry_bg->mRootNode, mesh_menu_died_retry_bg);
		else if (game_menu == 4)
			recursive_render(menu_died_quit_bg, menu_died_quit_bg->mRootNode, mesh_menu_died_quit_bg);
		recursive_render(wtc, wtc->mRootNode, mesh_wtc);
	}
	glfwSwapBuffers(window);
}
