/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   load_props.cpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/10 14:01:12 by cnolte            #+#    #+#             */
/*   Updated: 2018/01/10 14:58:13 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ftrun.hpp"

static const string world_1_name = "resources/props/world_north-south.obj";
static const string world_2_name = "resources/props/world_east-west.obj";
static const string imac_name = "resources/props/Imac2012.obj";
static const string table_double_name = "resources/props/table_double.obj";
static const string table_triple_name = "resources/props/table_triple.obj";
static const string chair_name = "resources/props/chair.obj";
static const string wtc_name = "resources/props/wtc.obj";
static const string menu_died_quit_bg_name = "resources/props/game_menu/menu_died_quit_bg.obj";
static const string menu_died_retry_bg_name = "resources/props/game_menu/menu_died_retry_bg.obj";
static const string menu_quit_bg_name = "resources/props/game_menu/menu_quit_bg.obj";
static const string menu_start_bg_name = "resources/props/game_menu/menu_start_bg.obj";

Assimp::Importer	import_world_1, import_world_2;
Assimp::Importer	import_imac[MAC_NS];
Assimp::Importer	import_table_double[TBL_D_NS];
Assimp::Importer	import_table_triple[TBL_T_NS];
Assimp::Importer	import_chair[CHAIR_NS];
Assimp::Importer	import_player_run[31];
Assimp::Importer	import_player_jump[34];
Assimp::Importer	import_player_fall[77];
Assimp::Importer	import_wtc;
Assimp::Importer	import_menu_died_quit_bg, import_menu_died_retry_bg, import_menu_quit_bg, import_menu_start_bg;

GLuint	matricesUniBuffer;
#define MatricesUniBufferSize sizeof(float) * 16 * 3

static void	*load_mac_data(void *ii)
{
	long i = (long)ii;
	imac[i] = import_imac[i].ReadFile(imac_name, aiProcessPreset_TargetRealtime_Fast | aiProcess_JoinIdenticalVertices);
	pthread_exit(NULL);
}

static void	*load_table_double_data(void *ii)
{
	long i = (long)ii;
	table_double[i] = import_table_double[i].ReadFile(table_double_name, aiProcessPreset_TargetRealtime_Fast | aiProcess_JoinIdenticalVertices);
	pthread_exit(NULL);
}

static void	*load_table_triple_data(void *ii)
{
	long i = (long)ii;
	table_triple[i] = import_table_triple[i].ReadFile(table_triple_name, aiProcessPreset_TargetRealtime_Fast | aiProcess_JoinIdenticalVertices);
	pthread_exit(NULL);
}

static void	*load_chair_data(void *ii)
{
	long i = (long)ii;
	chair[i] = import_chair[i].ReadFile(chair_name, aiProcessPreset_TargetRealtime_Fast);
	pthread_exit(NULL);
}

static void	*init_move_fall_player(void *ii)
{
	long i = (long)ii;
	move(&player_fall[i], 2, 30, -300);
	pthread_exit(NULL);
}

static void	*init_move_jump_player(void *ii)
{
	long i = (long)ii;
	move(&player_jump[i], 2, 30, -300);
	pthread_exit(NULL);
}

static void	*init_move_run_player(void *ii)
{
	long i = (long)ii;
	move(&player_run[i], 0, 30, -300);
	pthread_exit(NULL);
}

static void	*load_player_fall_data(void *ii)
{
	long i = (long)ii;
	player_fall[i] = import_player_fall[i].ReadFile("resources/props/player_fall/fall_" + to_string(i) + ".obj", aiProcessPreset_TargetRealtime_Fast | aiProcess_JoinIdenticalVertices);
	pthread_exit(NULL);
}

static void	*load_player_jump_data(void *ii)
{
	long i = (long)ii;
	player_jump[i] = import_player_jump[i].ReadFile("resources/props/player_jumping/jump_" + to_string(i) + ".obj", aiProcessPreset_TargetRealtime_Fast | aiProcess_JoinIdenticalVertices);
	pthread_exit(NULL);
}

static void	*load_player_run_data(void *ii)
{
	long i = (long)ii;
	player_run[i] = import_player_run[i].ReadFile("resources/props/player_running/run_" + to_string(i) + ".obj", aiProcessPreset_TargetRealtime_Fast | aiProcess_JoinIdenticalVertices);
	pthread_exit(NULL);
}

static void	load_without_threading(void)
{
	for (int j = 0; j < MAC_NS; j++)
		imac[j] = import_imac[j].ReadFile(imac_name, aiProcessPreset_TargetRealtime_Fast | aiProcess_JoinIdenticalVertices);
	
	for (int j = 0; j < TBL_D_NS; j++)
		table_double[j] = import_table_double[j].ReadFile(table_double_name, aiProcessPreset_TargetRealtime_Fast | aiProcess_JoinIdenticalVertices);
	
	for (int j = 0; j < TBL_T_NS; j++)
		table_triple[j] = import_table_triple[j].ReadFile(table_triple_name, aiProcessPreset_TargetRealtime_Fast | aiProcess_JoinIdenticalVertices);

	for (int j = 0; j < CHAIR_NS; j++)
		chair[j] = import_chair[j].ReadFile(chair_name, aiProcessPreset_TargetRealtime_Fast);

	for (int j = 0; j <= 30; j++)
		player_run[j] = import_player_run[j].ReadFile("resources/props/player_running/run_" + to_string(j) + ".obj", aiProcessPreset_TargetRealtime_Fast | aiProcess_JoinIdenticalVertices);

	for (int j = 0; j <= 33; j++)
		player_jump[j] = import_player_jump[j].ReadFile("resources/props/player_jumping/jump_" + to_string(j) + ".obj", aiProcessPreset_TargetRealtime_Fast | aiProcess_JoinIdenticalVertices);
	
	for (int j = 0; j <= 76; j++)
		player_fall[j] = import_player_fall[j].ReadFile("resources/props/player_fall/fall_" + to_string(j) + ".obj", aiProcessPreset_TargetRealtime_Fast | aiProcess_JoinIdenticalVertices);

	for (int k = 0; k <= 30; k++)
		move(&player_run[k], 0, 30, -300);

	for (int k = 0; k <= 33; k++)
		move(&player_jump[k], 0, 30, -300);
	
	for (int k = 0; k <= 76; k++)
		move(&player_fall[k], 0, 30, -300);
}

static void	load_with_threading(void)
{
	pthread_t		threads_run[31];
	pthread_t		threads_jump[34];
	pthread_t		threads_fall[77];
	pthread_t		threads_MAC_NS[MAC_NS];
	pthread_t		threads_TBL_D_NS[TBL_D_NS];
	pthread_t		threads_TBL_T_NS[TBL_T_NS];
	pthread_t		threads_CHAIR_NS[CHAIR_NS];
	pthread_attr_t	attr;
	void			*status;

	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	for (int j = 0; j < MAC_NS; j++)
		pthread_create(&threads_MAC_NS[j], NULL, load_mac_data, (void *)j);

	pthread_attr_destroy(&attr);
	for(int n = 0; n < MAC_NS; n++ )
		pthread_join(threads_MAC_NS[n], &status);
	
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	for (int j = 0; j < TBL_D_NS; j++)
		pthread_create(&threads_TBL_D_NS[j], NULL, load_table_double_data, (void *)j);

	pthread_attr_destroy(&attr);
	for(int n = 0; n < TBL_D_NS; n++ )
		pthread_join(threads_TBL_D_NS[n], &status);
	
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	for (int j = 0; j < TBL_T_NS; j++)
		pthread_create(&threads_TBL_T_NS[j], NULL, load_table_triple_data, (void *)j);

	pthread_attr_destroy(&attr);
	for(int n = 0; n < TBL_T_NS; n++ )
		pthread_join(threads_TBL_T_NS[n], &status);
	
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	for (int j = 0; j < CHAIR_NS; j++)
		pthread_create(&threads_CHAIR_NS[j], NULL, load_chair_data, (void *)j);

	pthread_attr_destroy(&attr);
	for(int n = 0; n < CHAIR_NS; n++ )
		pthread_join(threads_CHAIR_NS[n], &status);

	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	for (int j = 0; j <= 30; j++)
		pthread_create(&threads_run[j], NULL, load_player_run_data, (void *)j);

	pthread_attr_destroy(&attr);
	for(int n = 0; n < 31; n++ )
		pthread_join(threads_run[n], &status);

	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	for (int j = 0; j <= 33; j++)
		pthread_create(&threads_jump[j], NULL, load_player_jump_data, (void *)j);
	
	pthread_attr_destroy(&attr);
	for(int n = 0; n <= 33; n++ )
		pthread_join(threads_jump[n], &status);

	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	for (int j = 0; j <= 76; j++)
		pthread_create(&threads_fall[j], NULL, load_player_fall_data, (void *)j);
	
	pthread_attr_destroy(&attr);
	for(int n = 0; n <= 76; n++ )
		pthread_join(threads_fall[n], &status);

	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	for (int k = 0; k <= 30; k++)
		pthread_create(&threads_run[k], NULL, init_move_run_player, (void *)k);

	pthread_attr_destroy(&attr);
	for(int n = 0; n <= 30; n++ )
		pthread_join(threads_run[n], &status);

	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	for (int k = 0; k <= 33; k++)
		pthread_create(&threads_jump[k], NULL, init_move_jump_player, (void *)k);

	pthread_attr_destroy(&attr);
	for(int n = 0; n <= 33; n++ )
		pthread_join(threads_jump[n], &status);

	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	for (int k = 0; k <= 76; k++)
		pthread_create(&threads_fall[k], NULL, init_move_fall_player, (void *)k);

	pthread_attr_destroy(&attr);
	for(int n = 0; n <= 76; n++ )
		pthread_join(threads_fall[n], &status);
}

static int	import_error_handeling(void)
{
	if(!world_1)
	{
		printf("%s\n", import_world_1.GetErrorString());
		return (-1);
	}
	if(!world_2)
	{
		printf("%s\n", import_world_2.GetErrorString());
		return (-1);
	}
	if(!wtc)
	{
		printf("%s\n", import_wtc.GetErrorString());
		return (-1);
	}
	if(!menu_died_quit_bg)
	{
		printf("%s\n", import_menu_died_quit_bg.GetErrorString());
		return (-1);
	}
	if(!menu_died_retry_bg)
	{
		printf("%s\n", import_menu_died_retry_bg.GetErrorString());
		return (-1);
	}
	if(!menu_quit_bg)
	{
		printf("%s\n", import_menu_quit_bg.GetErrorString());
		return (-1);
	}
	if(!menu_start_bg)
	{
		printf("%s\n", import_menu_start_bg.GetErrorString());
		return (-1);
	}
	return (0);
}

int	load_set_opengl_data(void)
{
	if (MULTITHREADING == 1)
		load_with_threading();
	else
		load_without_threading();
	world_1 = import_world_1.ReadFile(world_1_name, aiProcessPreset_TargetRealtime_Fast | aiProcess_JoinIdenticalVertices);
	world_2 = import_world_2.ReadFile(world_2_name, aiProcessPreset_TargetRealtime_Fast | aiProcess_JoinIdenticalVertices);
	wtc = import_wtc.ReadFile(wtc_name, aiProcessPreset_TargetRealtime_Fast | aiProcess_JoinIdenticalVertices);
	menu_died_quit_bg = import_menu_died_quit_bg.ReadFile(menu_died_quit_bg_name, aiProcessPreset_TargetRealtime_Fast | aiProcess_JoinIdenticalVertices);
	menu_died_retry_bg = import_menu_died_retry_bg.ReadFile(menu_died_retry_bg_name, aiProcessPreset_TargetRealtime_Fast | aiProcess_JoinIdenticalVertices);
	menu_quit_bg = import_menu_quit_bg.ReadFile(menu_quit_bg_name, aiProcessPreset_TargetRealtime_Fast | aiProcess_JoinIdenticalVertices);
	menu_start_bg = import_menu_start_bg.ReadFile(menu_start_bg_name, aiProcessPreset_TargetRealtime_Fast | aiProcess_JoinIdenticalVertices);
	if (import_error_handeling())
		return (-1);
	load_textures();
	glGetUniformBlockIndex = (PFNGLGETUNIFORMBLOCKINDEXPROC) glfwGetProcAddress("glGetUniformBlockIndex");
	glUniformBlockBinding = (PFNGLUNIFORMBLOCKBINDINGPROC) glfwGetProcAddress("glUniformBlockBinding");
	glGenVertexArrays = (PFNGLGENVERTEXARRAYSPROC) glfwGetProcAddress("glGenVertexArrays");
	glBindVertexArray = (PFNGLBINDVERTEXARRAYPROC)glfwGetProcAddress("glBindVertexArray");
	glBindBufferRange = (PFNGLBINDBUFFERRANGEPROC) glfwGetProcAddress("glBindBufferRange");
	glDeleteVertexArrays = (PFNGLDELETEVERTEXARRAYSPROC) glfwGetProcAddress("glDeleteVertexArrays");
	initial_move();
	program = setup_shader();
	add_objects_to_gpu_memory();
	glEnable(GL_DEPTH_TEST);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glGenBuffers(1,&matricesUniBuffer);
	glBindBuffer(GL_UNIFORM_BUFFER, matricesUniBuffer);
	glBufferData(GL_UNIFORM_BUFFER, MatricesUniBufferSize,NULL,GL_DYNAMIC_DRAW);
	glBindBufferRange(GL_UNIFORM_BUFFER, matricesUniLoc, matricesUniBuffer, 0, MatricesUniBufferSize);	//setUniforms();
	glBindBuffer(GL_UNIFORM_BUFFER,0);
	glEnable(GL_MULTISAMPLE);
	return (0);
}
