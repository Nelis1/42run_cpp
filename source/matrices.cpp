/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   matrices.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/10 15:10:53 by cnolte            #+#    #+#             */
/*   Updated: 2018/01/10 15:51:35 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ftrun.hpp"

#define VIEWMATRIXOFFSET sizeof(float) * 16
#define MATRIX_SIZE sizeof(float) * 16
#define PROJMATRIXOFFSET 0
#define MODEL_MATRIX_OFFSET sizeof(float) * 16 * 2

vector<float *> matrix_stack;

// res = a cross b;
void cross_product( float *a, float *b, float *res) {

	res[0] = a[1] * b[2]  -  b[1] * a[2];
	res[1] = a[2] * b[0]  -  b[2] * a[0];
	res[2] = a[0] * b[1]  -  b[0] * a[1];
}

// Normalize a vec3
void normalize(float *a) {

	float mag = sqrt(a[0] * a[0] + a[1] * a[1] + a[2] * a[2]);

	a[0] /= mag;
	a[1] /= mag;
	a[2] /= mag;
}

void	set_identity_matrix(float *mat, int size)
{
	// fill matrix with 0s
	for (int i = 0; i < size * size; ++i)
			mat[i] = 0.0f;
	// fill diagonal with 1s
	for (int i = 0; i < size; ++i)
		mat[i + i * size] = 1.0f;
}

//
// a = a * b;
//
void	mult_matrix(float *a, float *b)
{
	float	res[16];

	for (int i = 0; i < 4; ++i)
	{
		for (int j = 0; j < 4; ++j)
		{
			res[j*4 + i] = 0.0f;
			for (int k = 0; k < 4; ++k)
				res[j*4 + i] += a[k*4 + i] * b[j*4 + k];
		}
	}
	memcpy(a, res, 16 * sizeof(float));
}

void	set_translation_matrix(float *mat, float x, float y, float z)
{
	set_identity_matrix(mat, 4);
	mat[12] = x;
	mat[13] = y;
	mat[14] = z;
}

// Defines a transformation matrix mat with a scale
void	set_scale_matrix(float *mat, float sx, float sy, float sz)
{
	set_identity_matrix(mat, 4);
	mat[0] = sx;
	mat[5] = sy;
	mat[10] = sz;
}

static float deg_to_rad(float degrees)
{
	return (float)(degrees * (M_PI / 180.0f));
}

// Defines a transformation matrix mat with a rotation 
// angle alpha and a rotation axis (x,y,z)
void	set_rotation_matrix(float *mat, float angle, float x, float y, float z)
{
	float	radAngle = deg_to_rad(angle);
	float	co = cos(radAngle);
	float	si = sin(radAngle);
	float	x2 = x * x;
	float	y2 = y * y;
	float	z2 = z * z;

	mat[0] = x2 + (y2 + z2) * co;
	mat[4] = x * y * (1 - co) - z * si;
	mat[8] = x * z * (1 - co) + y * si;
	mat[12] = 0.0f;

	mat[1] = x * y * (1 - co) + z * si;
	mat[5] = y2 + (x2 + z2) * co;
	mat[9] = y * z * (1 - co) - x * si;
	mat[13] = 0.0f;

	mat[2] = x * z * (1 - co) - y * si;
	mat[6] = y * z * (1 - co) + x * si;
	mat[10] = z2 + (x2 + y2) * co;
	mat[14] = 0.0f;

	mat[3] = 0.0f;
	mat[7] = 0.0f;
	mat[11] = 0.0f;
	mat[15] = 1.0f;
}

// ----------------------------------------------------
// Model Matrix 
//
// Copies the modelMatrix to the uniform buffer
void	set_model_matrix(void)
{
	glBindBuffer(GL_UNIFORM_BUFFER, matricesUniBuffer);
	glBufferSubData(GL_UNIFORM_BUFFER, MODEL_MATRIX_OFFSET, MATRIX_SIZE, model_matrix);
	glBindBuffer(GL_UNIFORM_BUFFER, 0);
}

// The equivalent to glRotate applied to the model matrix
void	rotate(float angle, float x, float y, float z)
{
	float	aux[16];

	set_rotation_matrix(aux, angle, x, y, z);
	mult_matrix(model_matrix,aux);
	set_model_matrix();
}

void	push_matrix(void)
{
	float *aux = (float *)malloc(sizeof(float) * 16);
	memcpy(aux, model_matrix, sizeof(float) * 16);
	matrix_stack.push_back(aux);
}

void	pop_matrix(void)
{
	float *m = matrix_stack[matrix_stack.size()-1];
	memcpy(model_matrix, m, sizeof(float) * 16);
	matrix_stack.pop_back();
	free(m);
}

void	set_camera(float pos_x, float pos_y, float pos_z, float look_at_x, float look_at_y, float look_at_z)
{
	float dir[3], right[3], up[3];

	up[0] = 0.0f;	up[1] = 1.0f;	up[2] = 0.0f;
	dir[0] =  (look_at_x - pos_x);
	dir[1] =  (look_at_y - pos_y);
	dir[2] =  (look_at_z - pos_z);
	normalize(dir);
	cross_product(dir, up, right);
	normalize(right);
	cross_product(right, dir, up);
	normalize(up);
	float	view_matrix[16];
	float	aux[16];

	view_matrix[0]  = right[0];
	view_matrix[4]  = right[1];
	view_matrix[8]  = right[2];
	view_matrix[12] = 0.0f;

	view_matrix[1]  = up[0];
	view_matrix[5]  = up[1];
	view_matrix[9]  = up[2];
	view_matrix[13] = 0.0f;

	view_matrix[2]  = -dir[0];
	view_matrix[6]  = -dir[1];
	view_matrix[10] = -dir[2];
	view_matrix[14] =  0.0f;

	view_matrix[3]  = 0.0f;
	view_matrix[7]  = 0.0f;
	view_matrix[11] = 0.0f;
	view_matrix[15] = 1.0f;

	set_translation_matrix(aux, -pos_x, -50, -pos_z);
	mult_matrix(view_matrix, aux);
	glBindBuffer(GL_UNIFORM_BUFFER, matricesUniBuffer);
	glBufferSubData(GL_UNIFORM_BUFFER, VIEWMATRIXOFFSET, MATRIX_SIZE, view_matrix);
	glBindBuffer(GL_UNIFORM_BUFFER, 0);
}

static void	build_projection_matrix(float fov, float ratio, float nearp, float farp)
{
	float	proj_matrix[16];
	float	f = 1.0f / tan (fov * (M_PI / 360.0f));

	set_identity_matrix(proj_matrix, 4);
	proj_matrix[0] = f / ratio;
	proj_matrix[1 * 4 + 1] = f;
	proj_matrix[2 * 4 + 2] = (farp + nearp) / (nearp - farp);
	proj_matrix[3 * 4 + 2] = (2.0f * farp * nearp) / (nearp - farp);
	proj_matrix[2 * 4 + 3] = -1.0f;
	proj_matrix[3 * 4 + 3] = 0.0f;
	glBindBuffer(GL_UNIFORM_BUFFER, matricesUniBuffer);
	glBufferSubData(GL_UNIFORM_BUFFER, PROJMATRIXOFFSET, MATRIX_SIZE, proj_matrix);
	glBindBuffer(GL_UNIFORM_BUFFER, 0);
}

void	create_projection_matrix(int w, int h)
{
	float	ratio;

	if(h == 0)
		h = 1;
	glViewport(0, 0, w, h);
	ratio = (1.0f * w) / h;
	build_projection_matrix(45.0f, ratio, 0.01f, 6190.0f);
}
