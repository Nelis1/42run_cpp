/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   process_return_key.cpp                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/09 18:42:08 by cnolte            #+#    #+#             */
/*   Updated: 2018/01/09 18:42:50 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ftrun.hpp"

void	process_return_key(void)
{
	if (game_menu == 2)
		glfwSetWindowShouldClose(window, GLFW_TRUE);
	else if (game_menu == 1)
	{
		Mix_PlayChannel( -1, audio.sound, 0 );
		SDL_Delay(2000);
		Mix_VolumeChunk(audio.sound3, MIX_MAX_VOLUME/3);
		Mix_PlayChannel( -1, audio.sound3, 0 );
		SDL_Delay(2000);
		Mix_PlayChannel( -1, audio.sound2, 0 );
		SDL_Delay(500);
		Mix_PlayChannel( -1, audio.sound1, -1 );
		game_menu = 0;
	}
	else if (game_menu == 4)
		glfwSetWindowShouldClose(window, GLFW_TRUE);
	if (game_menu == 3)
	{
		if (move_north_south >= 0)
		{
			for (int i = 0; i < MAC_N; i++)
			{
				move(&imac[i], 0, 0, 63.2 * (-(move_north_south - 9)));
				update_vao(imac[i], &mesh_imac[i]);
			}
			for (int i = 0; i < TBL_D_N; i++)
			{
				move(&table_double[i], 0, 0, 63.2 * (-(move_north_south - 9)));
				update_vao(table_double[i], &mesh_table_double[i]);
			}
			for (int i = 0; i < TBL_T_N; i++)
			{
				move(&table_triple[i], 0, 0, 63.2 * (-(move_north_south - 9)));
				update_vao(table_triple[i], &mesh_table_triple[i]);
			}
			for (int i = 0; i < CHAIR_N; i++)
			{
				move(&chair[i], 0, 0, 63.2 * (-(move_north_south - 9)));
				update_vao(chair[i], &mesh_chair[i]);
			}
		}
		else
		{
			if (move_north_south < 0)
			{
				for (int i = 0; i < MAC_N; i++)
				{
					move(&imac[i], 0, 0, 63.2 * 9);
					update_vao(imac[i], &mesh_imac[i]);
				}
				for (int i = 0; i < TBL_D_N; i++)
				{
					move(&table_double[i], 0, 0, 63.2 * 9);
					update_vao(table_double[i], &mesh_table_double[i]);
				}
				for (int i = 0; i < TBL_T_N; i++)
				{
					move(&table_triple[i], 0, 0, 63.2 * 9);
					update_vao(table_triple[i], &mesh_table_triple[i]);
				}
				for (int i = 0; i < CHAIR_N; i++)
				{
					move(&chair[i], 0, 0, 63.2 * 9);
					update_vao(chair[i], &mesh_chair[i]);
				}
				for (int i = MAC_N; i < MAC_NS; i++)
				{
					move(&imac[i], 0, 0, 63.2 * (move_north_south));
					update_vao(imac[i], &mesh_imac[i]);
				}
				for (int i = TBL_D_N; i < TBL_D_NS; i++)
				{
					move(&table_double[i], 0, 0, 63.2 * (move_north_south));
					update_vao(table_double[i], &mesh_table_double[i]);
				}
				for (int i = TBL_T_N; i < TBL_T_NS; i++)
				{
					move(&table_triple[i], 0, 0, 63.2 * (move_north_south));
					update_vao(table_triple[i], &mesh_table_triple[i]);
				}
				for (int i = CHAIR_N; i < CHAIR_NS; i++)
				{
					move(&chair[i], 0, 0, 63.2 * (move_north_south));
					update_vao(chair[i], &mesh_chair[i]);
				}
			}
		}
		move(&world_1, 0, 0, 63.2 * (-(move_north_south - 9)));
		update_vao(world_1, &mesh_world_1);
		if (fall == 0)
		{
			move(&world_2, 63.2 * move_east_west, 0, 0);
			update_vao(world_2, &mesh_world_2);
		}
		if (p_pos != 0)
		{
			for (int i = 0; i <= 33; i++)
			{
				move(&player_jump[i], -115 * p_pos, 0.0, 0.0);
				update_vao(player_jump[i], &mesh_player_jump[i]);
			}
			for (int i = 0; i <= 30; i++)
			{
				move(&player_run[i], -115.0 * p_pos, 0.0, 0.0);
				update_vao(player_run[i], &mesh_player_run[i]);
			}
			for (int i = 0; i <= 76; i++)
			{
				move(&player_fall[i], -115.0 * p_pos, 0.0, 0.0);
				update_vao(player_fall[i], &mesh_player_fall[i]);
			}
		}
		score = 0;
		p_pos = 0;
		dir = 0;
		rot = 0;
		rot_play = 0;
		move_east_west = 0;
		move_north_south = 9;
		h_neg = 0;
		v_neg = 0;
		jump = 0;
		fall = 0;
		next_turn = -1;
		game_menu = 0;
	}
}
