/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   process_right_key.cpp                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/09 18:36:40 by cnolte            #+#    #+#             */
/*   Updated: 2018/01/09 18:37:37 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ftrun.hpp"

void	process_right_key(void)
{
	if (rot_play == -180 && move_north_south < -80)
	{
		if (DEBUG == 0)
			next_turn = -90;
		else if (DEBUG == 1)
		{
			rot_play = -90;
			rot = 3;
		}
	}
	else if (rot_play == -90 && move_east_west < -80)
	{
		if (DEBUG == 0)
			next_turn = 0;
		else if (DEBUG == 1)
		{
			rot_play = 0;
			rot = 3;
		}
	}
	else if (rot_play == 0 && move_north_south > 80)
	{
		if (DEBUG == 0)
			next_turn = 90;
		else if (DEBUG == 1)
		{
			rot_play = 90;
			rot = 3;
		}
	}
	else if (rot_play == 90 && move_east_west > 80)
	{
		if (DEBUG == 0)
			next_turn = 180;
		else if (DEBUG == 1)
		{
			rot_play = 180;
			rot = 3;
		}
	}
	else if (rot_play == 180 && move_north_south < -80)
	{
		if (DEBUG == 0)
			next_turn = 270;
		else if (DEBUG == 1)
		{
			rot_play = 270;
			rot = 3;
		}
	}
}
