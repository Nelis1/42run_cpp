/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main_key_event.cpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/09 18:32:12 by cnolte            #+#    #+#             */
/*   Updated: 2018/02/13 18:22:18 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ftrun.hpp"

void process_keys(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	switch(key)
	{
		case GLFW_KEY_SPACE:
			jump = 1;
			break;
		case GLFW_KEY_ESCAPE:
			if (action == GLFW_PRESS)
				glfwSetWindowShouldClose(window, GLFW_TRUE);
			break;
		case GLFW_KEY_LEFT:
			if (action == GLFW_PRESS)
				process_left_key();
			break;
		case GLFW_KEY_RIGHT:
			if (action == GLFW_PRESS)
				process_right_key();
			break;
		case GLFW_KEY_UP:
			if (action == GLFW_PRESS)
				process_up_key();
			break;
		case GLFW_KEY_DOWN:
			if (action == GLFW_PRESS)
				process_down_key();
			break;
		case GLFW_KEY_ENTER:
			if (action == GLFW_PRESS)
				process_return_key();
			break;
		case GLFW_KEY_X:
			if (action == GLFW_PRESS)
			{
				if (p_pos < 1 && fall == 0 && jump == 0)
				{
					for (int i = 0; i <= 33; i++)
					{
						move(&player_jump[i], 115, 0.0, 0.0);
						update_vao(player_jump[i], &mesh_player_jump[i]);
					}
					for (int i = 0; i <= 30; i++)
					{
						move(&player_run[i], 115.0, 0.0, 0.0);
						update_vao(player_run[i], &mesh_player_run[i]);
					}
					for (int i = 0; i <= 76; i++)
					{
						move(&player_fall[i], 115.0, 0.0, 0.0);
						update_vao(player_fall[i], &mesh_player_fall[i]);
					}
					p_pos++;
				}
			}
			break;
		case GLFW_KEY_Z:
			if (action == GLFW_PRESS)
			{
				if (p_pos > -1 && fall == 0 && jump == 0)
				{
					for (int i = 0; i <= 33; i++)
					{
						move(&player_jump[i], -115, 0.0, 0.0);
						update_vao(player_jump[i], &mesh_player_jump[i]);
					}
					for (int i = 0; i <= 30; i++)
					{
						move(&player_run[i], -115.0, 0.0, 0.0);
						update_vao(player_run[i], &mesh_player_run[i]);
					}
					for (int i = 0; i <= 76; i++)
					{
						move(&player_fall[i], -115.0, 0.0, 0.0);
						update_vao(player_fall[i], &mesh_player_fall[i]);
					}
					p_pos--;
				}
			}
			break;
	}
}
