/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   process_up_key.cpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/09 18:38:12 by cnolte            #+#    #+#             */
/*   Updated: 2018/01/09 18:39:01 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ftrun.hpp"

void	process_up_key(void)
{
	if (DEBUG == 1 && game_menu == 0)
	{
		printf("move_north-south = %d\n", move_north_south);
		printf("move_east-west = %d\n", move_east_west);
		forward_movement();
	}
	else if (game_menu == 2)
		game_menu = 1;
	else if (game_menu == 4)
		game_menu = 3;
}
