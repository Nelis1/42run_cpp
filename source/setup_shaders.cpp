/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   setup_shaders.cpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/10 14:18:11 by cnolte            #+#    #+#             */
/*   Updated: 2018/01/10 14:39:45 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ftrun.hpp"

GLuint	vertexShader;
GLuint	fragmentShader;

static char *textFileRead(char *fn)
{
	FILE *fp;
	char *content = NULL;
	int count=0;

	if (fn != NULL)
	{
		fp = fopen(fn,"rt");
		if (fp != NULL)
		{
			fseek(fp, 0, SEEK_END);
			count = ftell(fp);
			rewind(fp);
			if (count > 0)
			{
				content = (char *)malloc(sizeof(char) * (count+1));
				count = fread(content,sizeof(char),count,fp);
				content[count] = '\0';
			}
			fclose(fp);
		}
	}
	return content;
}

GLuint	setup_shader(void)
{
	GLuint	texUnit = 0;
	GLuint	p, v, f;

	char *vs = NULL;
	char *fs = NULL;
	char *fs2 = NULL;

	v = glCreateShader(GL_VERTEX_SHADER);
	f = glCreateShader(GL_FRAGMENT_SHADER);

	vs = textFileRead("resources/shaders/dirlightdiffambpix.vert");
	fs = textFileRead("resources/shaders/dirlightdiffambpix.frag");
	const char *vv = vs;
	const char *ff = fs;
	glShaderSource(v, 1, &vv, NULL);
	glShaderSource(f, 1, &ff, NULL);
	free(vs);free(fs);
	glCompileShader(v);
	glCompileShader(f);
	p = glCreateProgram();
	glAttachShader(p, v);
	glAttachShader(p, f);
	glBindFragDataLocation(p, 0, "out_color");
	glBindAttribLocation(p,vertexLoc, "position");
	glBindAttribLocation(p,normalLoc, "normal");
	glBindAttribLocation(p,texCoordLoc, "texCoord");
	glLinkProgram(p);
	glValidateProgram(p);
	program = p;
	vertexShader = v;
	fragmentShader = f;
	GLuint k = glGetUniformBlockIndex(p, "Matrices");
	glUniformBlockBinding(p, k, matricesUniLoc);
	glUniformBlockBinding(p, glGetUniformBlockIndex(p, "Material"), materialUniLoc);
	texUnit = glGetUniformLocation(p,"texUnit");
	return (p);
}
