/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   process_turn.cpp                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/10 16:08:34 by cnolte            #+#    #+#             */
/*   Updated: 2018/01/10 16:12:50 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ftrun.hpp"

void	process_right_turn(int r)
{
	if (DEBUG == 1)
		printf("turn right\n");
	for (int i = 0; i <= 30; i++)
	{
		move(&player_run[i], 0.0, 0.0, 125.0);
		update_vao(player_run[i], &mesh_player_run[i]);
	}
	if (move_north_south == 90)
	{
		move(&world_1, 0.0, 0.0, 145.0);
		update_vao(world_1, &mesh_world_1);
	}
	else if (move_north_south == -90)
	{
		move(&world_1, 0.0, 0.0, -145.0);
		update_vao(world_1, &mesh_world_1);
	}
	if (move_east_west == 90)
	{
		move(&world_2, -145.0, 0.0, 0.0);
		update_vao(world_2, &mesh_world_2);
	}
	else if (move_east_west == -90)
	{
		move(&world_2, 145.0, 0.0, 0.0);
		update_vao(world_2, &mesh_world_2);
	}
	while (dir < rot_play)
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		rotate((float)dir, 0.0, 1.0, 0.0);
		recursive_render(world_1, world_1->mRootNode, mesh_world_1);
		recursive_render(world_2, world_2->mRootNode, mesh_world_2);
		rotate(-(float)dir, 0.0, 1.0, 0.0);
		recursive_render(player_run[r], player_run[r]->mRootNode, mesh_player_run[r]);
		glFinish();
		if (dir < rot_play - ROT_SPEED)
			glfwSwapBuffers(window);
		dir += ROT_SPEED;
	}
	for (int i = 0; i <= 30; i++)
	{
		move(&player_run[i], 0.0, 0.0, -125.0);
		update_vao(player_run[i], &mesh_player_run[i]);
	}
	if (rot_play == 270)
	{
		if (DEBUG == 1)
			printf("reset\n");
		rot_play = -90;
		dir = -90;
	}
	rot = 0;
}

void	process_left_turn(int r)
{
	if (DEBUG == 1)
		printf("turn left\n");
	for (int i = 0; i <= 30; i++)
	{
		move(&player_run[i], 0.0, 0.0, 125.0);
		update_vao(player_run[i], &mesh_player_run[i]);
	}
	if (move_north_south == 90)
	{
		move(&world_1, 0.0, 0.0, 145.0);
		update_vao(world_1, &mesh_world_1);
	}
	else if (move_north_south == -90)
	{
		move(&world_1, 0.0, 0.0, -145.0);
		update_vao(world_1, &mesh_world_1);
	}
	if (move_east_west == 90)
	{
		move(&world_2, -145.0, 0.0, 0.0);
		update_vao(world_2, &mesh_world_2);
	}
	else if (move_east_west == -90)
	{
		move(&world_2, 145.0, 0.0, 0.0);
		update_vao(world_2, &mesh_world_2);
	}
	while (dir > rot_play)
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		rotate((float)dir, 0.0, 1.0, 0.0);
		recursive_render(world_1, world_1->mRootNode, mesh_world_1);
		recursive_render(world_2, world_2->mRootNode, mesh_world_2);
		rotate(-(float)dir, 0.0, 1.0, 0.0);
		recursive_render(player_run[r], player_run[r]->mRootNode, mesh_player_run[r]);
		glFinish();
		if (dir > rot_play + ROT_SPEED)
			glfwSwapBuffers(window);
		dir -= ROT_SPEED;
	}
	for (int i = 0; i <= 30; i++)
	{
		move(&player_run[i], 0.0, 0.0, -125.0);
		update_vao(player_run[i], &mesh_player_run[i]);
	}
	if (rot_play == -270)
	{
		if (DEBUG == 1)
			printf("reset\n");
		rot_play = 90;
		dir = 90;
	}
	rot = 0;
}