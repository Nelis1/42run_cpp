/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move_south.cpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/09 17:31:47 by cnolte            #+#    #+#             */
/*   Updated: 2018/01/09 18:04:00 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ftrun.hpp"

static void	process_t_junction(void)
{
	if (DEBUG == 0)
	{
		if (next_turn == -1)
			game_menu = 3;
		else
		{
			if (next_turn == -270)
				rot = 2;
			else if (next_turn == 90)
				rot = 2;
			else
				rot = 3;
			rot_play = next_turn;
			next_turn = -1;
		}
	}
	v_neg = 1;
	move_east_west = 0;
}

static void	move_objects(void)
{
	move(&world_1, 0, 0, -63.2);
	update_vao(world_1, &mesh_world_1);
	for (int i = MAC_N; i < MAC_NS; i++)
	{
		move(&imac[i], 0, 0, 63.2);
		update_vao(imac[i], &mesh_imac[i]);
	}
	for (int i = TBL_D_N; i < TBL_D_NS; i++)
	{
		move(&table_double[i], 0, 0, 63.2);
		update_vao(table_double[i], &mesh_table_double[i]);
	}
	for (int i = TBL_T_N; i < TBL_T_NS; i++)
	{
		move(&table_triple[i], 0, 0, 63.2);
		update_vao(table_triple[i], &mesh_table_triple[i]);
	}
	for (int i = CHAIR_N; i < CHAIR_NS; i++)
	{
		move(&chair[i], 0, 0, 63.2);
		update_vao(chair[i], &mesh_chair[i]);
	}
}

static void	world_sync(void)
{
	if (move_north_south > -8)
	{
		move(&world_2, 0, 0, -64.5);
		update_vao(world_2, &mesh_world_2);
	}
	else if (move_north_south == -8)
	{
		if (h_neg == 1)
			move(&world_2, -5833.0, 0, 516.0);
		else if (h_neg == 0)
			move(&world_2, 5833.0, 0, 516.0);
		update_vao(world_2, &mesh_world_2);
	}
}

void	move_south_func(void)
{
	game_speed = 0.12;
	if (move_north_south > -90 && (move_east_west == 0 || move_east_west == 90 || move_east_west == -90))
	{
		world_sync();
		move_objects();
		move_north_south--;
		score++;
	}
	else if (move_north_south == -90)
		process_t_junction();
}
