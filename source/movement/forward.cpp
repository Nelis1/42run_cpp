/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   forward.cpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/31 19:09:12 by cnolte            #+#    #+#             */
/*   Updated: 2018/01/09 18:09:25 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ftrun.hpp"

void	forward_movement(void)
{
	if (rot_play == 0)
		move_north_func();
	else if (rot_play == 90)
		move_east_func();
	else if (rot_play == 180 || rot_play == -180)
		move_south_func();
	else if (rot_play == -90)
		move_west_func();
	string score_tmp;
	score_tmp = WIN_CAPTION + string(" | Current score: ") + to_string(score) + string("-Meters");
	glfwSetWindowTitle(window, score_tmp.c_str());
}
