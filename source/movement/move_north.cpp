/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move_north.cpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/09 17:29:49 by cnolte            #+#    #+#             */
/*   Updated: 2018/01/09 18:09:36 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ftrun.hpp"

static void	process_t_junction(void)
{
	if (DEBUG == 0)
	{
		if (next_turn == -1)
			game_menu = 3;
		else
		{
			if (next_turn > rot_play)
				rot = 3;
			else
				rot = 2;
			rot_play = next_turn;
			next_turn = -1;
		}
	}
	v_neg = 0;
	move_east_west = 0;
}

static void	update_movements(void)
{
	update_vao(world_1, &mesh_world_1);
	for (int i = 0; i < TBL_D_N; i++)
		update_vao(table_double[i], &mesh_table_double[i]);
	for (int i = 0; i < TBL_T_N; i++)
		update_vao(table_triple[i], &mesh_table_triple[i]);
	for (int i = 0; i < MAC_N; i++)
		update_vao(imac[i], &mesh_imac[i]);
	for (int i = 0; i < CHAIR_N; i++)
		update_vao(chair[i], &mesh_chair[i]);
}

static void	move_objects(void)
{
	pthread_t		threads[5];
	pthread_attr_t	attr;
	void			*status;
	
	if (MULTITHREADING == 1)
	{
		pthread_attr_init(&attr);
		pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
		for(int i = 0; i <= 4; i++ )
			pthread_create(&threads[i], NULL, moveThread, (void *)i);
		pthread_attr_destroy(&attr);
		for(int i = 0; i <= 4; i++ )
			pthread_join(threads[i], &status);
	}
	else
	{
		move(&world_1, 0, 0, 63.2);
		for (int i = 0; i < TBL_D_N; i++)
			move(&table_double[i], 0, 0, 63.2);
		for (int i = 0; i < TBL_T_N; i++)
			move(&table_triple[i], 0, 0, 63.2);
		for (int i = 0; i < MAC_N; i++)
			move(&imac[i], 0, 0, 63.2);
		for (int i = 0; i < CHAIR_N; i++)
			move(&chair[i], 0, 0, 63.2);
	}
}

static void	world_sync(void)
{
	if (move_north_south < 8)
	{
		move(&world_2, 0, 0, 64.5);
		update_vao(world_2, &mesh_world_2);
	}
	else if (move_north_south == 8)
	{
		if (h_neg == 1)
			move(&world_2, -5833.0, 0.0, -516.0);
		else if (h_neg == 0)
			move(&world_2, 5833.0, 0.0, -516.0);
		update_vao(world_2, &mesh_world_2);
	}
}

void	move_north_func(void)
{
	game_speed = 0.05;
	if (move_north_south < 90 && (move_east_west == 0 || move_east_west == 90 || move_east_west == -90))
	{
		world_sync();
		move_objects();
		update_movements();
		move_north_south++;
		score++;
	}
	else if (move_north_south == 90)
		process_t_junction();
}
