/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move_east.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/09 17:34:07 by cnolte            #+#    #+#             */
/*   Updated: 2018/01/09 18:01:27 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ftrun.hpp"

static void	process_t_junction(void)
{
	if (DEBUG == 0)
	{
		if (next_turn == -1)
			game_menu = 3;
		else
		{
			if (next_turn > rot_play)
				rot = 3;
			else
				rot = 2;
			rot_play = next_turn;
			next_turn = -1;
		}
	}
	h_neg = 0;
	move_north_south = 0;
}

static void	process_vertical_is_negative(void)
{
	move(&world_1, 516.0, 0, 5833.0);
	if (move_north_south == 90)
	{
		for (int i = 0; i < MAC_N; i++)
		{
			move(&imac[i], 0, 0, 5688.0);
			update_vao(imac[i], &mesh_imac[i]);
		}
		for (int i = 0; i < TBL_D_N; i++)
		{
			move(&table_double[i], 0, 0, 5688.0);
			update_vao(table_double[i], &mesh_table_double[i]);
		}
		for (int i = 0; i < TBL_T_N; i++)
		{
			move(&table_triple[i], 0, 0, 5688.0);
			update_vao(table_triple[i], &mesh_table_triple[i]);
		}
		for (int i = 0; i < CHAIR_N; i++)
		{
			move(&chair[i], 0, 0, 5688.0);
			update_vao(chair[i], &mesh_chair[i]);
		}
	}
	else if (move_north_south == -90)
	{
		for (int i = MAC_N; i < MAC_NS; i++)
		{
			move(&imac[i], 0, 0, -5688.0);
			update_vao(imac[i], &mesh_imac[i]);
		}
		for (int i = TBL_D_N; i < TBL_D_NS; i++)
		{
			move(&table_double[i], 0, 0, -5688.0);
			update_vao(table_double[i], &mesh_table_double[i]);
		}
		for (int i = TBL_T_N; i < TBL_T_NS; i++)
		{
			move(&table_triple[i], 0, 0, -5688.0);
			update_vao(table_triple[i], &mesh_table_triple[i]);
		}
		for (int i = CHAIR_N; i < CHAIR_NS; i++)
		{
			move(&chair[i], 0, 0, -5688.0);
			update_vao(chair[i], &mesh_chair[i]);
		}
	}
}

static void	process_vertical_is_positive(void)
{
	move(&world_1, 516.0, 0, -5833.0);
	if (move_north_south == 90)
	{
		for (int i = 0; i < MAC_N; i++)
		{
			move(&imac[i], 0, 0, -5688.0);
			update_vao(imac[i], &mesh_imac[i]);
		}
		for (int i = 0; i < TBL_D_N; i++)
		{
			move(&table_double[i], 0, 0, -5688.0);
			update_vao(table_double[i], &mesh_table_double[i]);
		}
		for (int i = 0; i < TBL_T_N; i++)
		{
			move(&table_triple[i], 0, 0, -5688.0);
			update_vao(table_triple[i], &mesh_table_triple[i]);
		}
		for (int i = 0; i < CHAIR_N; i++)
		{
			move(&chair[i], 0, 0, -5688.0);
			update_vao(chair[i], &mesh_chair[i]);
		}
	}
	else if (move_north_south == -90)
	{
		for (int i = MAC_N; i < MAC_NS; i++)
		{
			move(&imac[i], 0, 0, 5688.0);
			update_vao(imac[i], &mesh_imac[i]);
		}
		for (int i = TBL_D_N; i < TBL_D_NS; i++)
		{
			move(&table_double[i], 0, 0, 5688.0);
			update_vao(table_double[i], &mesh_table_double[i]);
		}
		for (int i = TBL_T_N; i < TBL_T_NS; i++)
		{
			move(&table_triple[i], 0, 0, 5688.0);
			update_vao(table_triple[i], &mesh_table_triple[i]);
		}
		for (int i = CHAIR_N; i < CHAIR_NS; i++)
		{
			move(&chair[i], 0, 0, 5688.0);
			update_vao(chair[i], &mesh_chair[i]);
		}
	}
}

static void	world_sync(void)
{
	if (move_east_west < 8)
	{
		move(&world_1, -64.5, 0, 0);
		update_vao(world_1, &mesh_world_1);
	}
	else if (move_east_west == 8)
	{
		if (v_neg == 0)
			process_vertical_is_positive();
		else if (v_neg == 1)
			process_vertical_is_negative();
		update_vao(world_1, &mesh_world_1);
	}
}

void	move_east_func(void)
{
	game_speed = 0.05;
	if (move_east_west < 90 && (move_north_south == 0 || move_north_south == 90 || move_north_south == -90))
	{
		world_sync();
		move(&world_2, -63.2, 0, 0);
		update_vao(world_2, &mesh_world_2);
		move_east_west++;
		score++;
	}
	else if (move_east_west == 90)
	{
		process_t_junction();
	}
}
